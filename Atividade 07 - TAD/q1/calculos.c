#include<stdio.h>
#include "calculos.h"

void soma(float num1, float num2, float* p_result){
    *p_result = num1+num2;
}

void subtrair(float num1, float num2, float* p_result){
    *p_result = num1 - num2;
}

void dividir(float num1, float num2, float* p_result){
    if(num2!=0){
        *p_result = num1 /num2;
    }
    else{
        printf("Impossível dividir por 0. Resultado inválido.");
    }
}
void multiplicar(float num1, float num2, float* p_result){
    *p_result = num1 * num2;
}
