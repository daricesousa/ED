#ifndef string_h
#define string_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int comprimento(char *str);
void copia(char *dest, char *orig);
void concatena(char *dest, char *orig);

#endif