#include "string.h"

int comprimento(char *str)
{
    int cont = 0;
    for (int i = 0; str[i] != '\0'; i++)
    {
        cont++;
    }
    return cont;
}
void copia(char *dest, char *orig)
{
    int n = comprimento(orig);
    dest = (char *)calloc(n, sizeof(char));
    for (int i = 0; i < n; i++)
    {
        *(dest + i) = *(orig + i);
    }
    printf("\nCopia: %s\n", dest);
}
void concatena(char *dest, char *orig)
{
    int tamOrig = comprimento(orig);
    int tamDest = comprimento(dest);
    dest = (char *)calloc(tamDest + tamOrig, sizeof(char));
    for (int i = 0; i < tamOrig; i++)
    {
        *(dest + tamDest + i) = *(orig + i);
    }
    printf("\n%s", dest);
}