#ifndef aluno_h
#define aluno_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct aluno Aluno;

void preencher(Aluno *aluno, int posicao, int mat, char turma, char nome[], float p1, float p2, float p3);
void buscar(Aluno *aluno, int tamanho, int matricula);
void ordenar(Aluno *aluno, int tamanho);
void imprimir(Aluno *aluno, int tamanho);

#endif