#ifndef circulo_h
#define circulo_h

#include <stdio.h>
#include <stdlib.h>

typedef struct Matriz Matriz;

void criar(Matriz *m, int linha, int coluna);
void imprimir(Matriz *matriz);
void liberar(Matriz *matriz);
void acessar(Matriz *matriz, int i, int j);
void atribuir(Matriz *matriz, int i, int j, int novoNumero);
void quantLinhas(Matriz *matriz);
void quantColunas(Matriz *matriz);

#endif