#ifndef circulo_h
#define circulo_h

#include <stdio.h>
#include <stdlib.h>

typedef struct Circulo Circulo;

Circulo* criar(Circulo *circulo, int x, int y, int raio);
void liberar(Circulo *circulo);
void calcularArea(Circulo *circulo, float *area);
void interior(Circulo *circulo, int x, int y);

#endif