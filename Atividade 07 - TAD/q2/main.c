#include <stdio.h>
#include "circulo.h"

void menu(int *op)
{
    do
    {
        printf("\n\nEscolha uma opção:\n");
        printf("1-Criar circulo;\n2-Liberar circulo;\n3-Ver área do circulo;\n4-Verificar se um ponto pertence ao circulo;\n5-Sair.\n");
        scanf("%d", &(*op));
    } while ((*op) < 1 || (*op) > 5);
}

void numeros(int *x, int *y)
{
    printf("\nDigite dois valores:\n");
    scanf("%d", &(*x));
    scanf("%d", &(*y));
}

void lerRaio(int *raio)
{
    printf("Digite o valor do raio:\n");
    scanf("%d", raio);
}

void main()
{
    int op;
    int x, y, raio;
    float area;
    Circulo *circulo;
    circulo = (Circulo *)calloc(1, sizeof(Circulo*));
    menu(&op);
    do
    {
        switch (op)
        {
        case 1:
            numeros(&x, &y);
            lerRaio(&raio);
            circulo = criar(circulo, x, y, raio);
            break;
        case 2:
            liberar(circulo);
            break;
        case 3:
        calcularArea(circulo, &area);
            printf("area: %f", area);
            break;
        case 4:
            numeros(&x, &y);
            interior(circulo, x, y);
            break;
        }
        menu(&op);
    } while (op != 5);
}