#ifndef biblioteca_h
#define biblioteca_h

typedef struct livro Livro;

Livro *livroCria(char titulo[], char autor[], char genero[], int ano);
char *livroObtemGenero(Livro *livro);
char *livroObtemAutor(Livro *livro);
char *livroObtemTitulo(Livro *livro);
int livroObtemAno(Livro *livro);
int verifica(Livro *livro);

#endif