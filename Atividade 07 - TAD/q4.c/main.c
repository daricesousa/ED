#include <stdio.h>
#include "matriz.h"

void menu(int *op)
{
    do
    {
        printf("\n\nEscolha uma opção:\n");
        printf("1-Criar matriz;\n2-Liberar matriz;\n3-Acessar elemento;\n4-Atribuir elemento;\n5-Linha;\n6-Colunas\n7-Imprimir matriz\n8-Sair.\n");
        scanf("%d", &(*op));
    } while ((*op) < 1 || (*op) > 8);
}

void numeros(int *linha, int *coluna)
{
    printf("\nDigite dois valores:\n");
    scanf("%d", &(*linha));
    scanf("%d", &(*coluna));
}
void main()
{
    int op;
    int linha, coluna;

    Matriz *matriz;
    menu(&op);
    do
    {
        switch (op)
        {
        case 1:
            numeros(&linha, &coluna);
            criar(matriz, linha, coluna);
            matrizQuadrada(matriz);
            break;
        case 2:
            liberar(matriz);
            break;
        case 3:
            numeros(&linha, &coluna);
            acessar(matriz, linha, coluna);
            break;
        case 4:
            numeros(&linha, &coluna);
            printf("Digite o novo número:\n");
            int novoNumero;
            scanf("%d", &novoNumero);
            atribuir(matriz, linha, coluna, novoNumero);
            break;
        case 5:
            quantLinhas(matriz);
            break;
         case 6:
            quantColunas(matriz);
            break;
        case 7:
            imprimir(matriz);
            break;
        }
        menu(&op);
    } while (op != 8);
}