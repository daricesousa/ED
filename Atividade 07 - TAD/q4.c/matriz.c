#include <stdio.h>
#include "matriz.h"
#include <math.h>

struct Matriz
{
    int linha, coluna;
    int **matriz;
};

void criar(Matriz *m, int linha, int coluna)
{
    m->matriz = (int **)calloc(linha, sizeof(int*));
    for (int i = 0; i < linha; i++)
    {
        m->matriz[i] = (int*) calloc(coluna,sizeof(int));
    }
    m->linha = linha;
    m->coluna =coluna;

    int cont = 0;
    for(int i=0; i<linha;i++){
        for(int j=0; j<coluna;j++){
            cont++;
            m->matriz[i][j] = cont;
        }
    }
}

void imprimir(Matriz *m){
    for(int i=0; i<(m->linha);i++){
        for(int j=0; j<(m->coluna);j++){
            printf("%d ", m->matriz[i][j]);
        }
        printf("\n");
    }
}
void liberar(Matriz *matriz)
{
    int linhas = matriz->linha;
    for (int i = 0; i < linhas; i++)
    {
        free(matriz->matriz[i]);
    }
    free(matriz->matriz);
}

void acessar(Matriz *matriz, int i, int j)
{   
    if(i>=0 && i<matriz->linha && j>=0 && j<matriz->coluna)
        printf("matriz[%d][%d]=%d\n", i, j, matriz->matriz[i][j]);
    else
        printf("Essa posição não existe nessa matriz");
    
}

void atribuir(Matriz *matriz, int i, int j, int novoNumero){
    if(i>=0 && i<matriz->linha && j>=0 && j<matriz->coluna)
        matriz->matriz[i][j] = novoNumero;
    else
        printf("Essa posição não existe nessa matriz");
}

void quantLinhas(Matriz *matriz){
    printf("A matriz tem %d linhas", matriz->linha);
}

void quantColunas(Matriz *matriz){
    printf("A matriz tem %d colunas", matriz->coluna);
}

void matrizQuadrada(Matriz *matriz){
    if(matriz->linha == matriz->coluna){
        printf("é uma matriz quadrada");
    }
    else{
        printf("Não é uma matriz quadrada");
    }
}