//1 – Desenvolva um algoritmo que calcule a média e a variancia, usando os
//protótipos das funções abaixo:
//• void capturar(int n, float *x); // alimentar o vetor e retornar no ponteiro *x
//• Float media(int n, float *x);
//• Float varianvia(int n, float *x, float m);
//• Ao final, o algoritmo deve mostrar o vetor preenchido, a média e a variance

#include <stdio.h>

void capturar(int n, float *x)
{
    for (int i; i < n; i++)
    {
        x[i] = i;
    }
}

float media(int n, float *x)
{
    float soma = 0;
    for (int i = 0; i < n; i++)
    {
        soma += x[i];
    }
    return soma / n;
}

float variancia(int n, float *x, float m)
{
    float soma = 0;
    for (int i = 0; i < n; i++)
    {
        soma += (x[i] - m) * (x[i] - m);
    }
    return soma / n;
}

void main()
{
    float x[10];
    capturar(10, x);
    float m = media(10, x);
    float v = variancia(10, x, m);

    for (int i = 0; i < 10; i++)
    {
        printf("%.2f, ", x[i]);
    }

    printf("\nmedia: %.1f\nvariancia: %.2f", m, v);
}