// • Crie um algoritmo que leia 3 notas de um aluno, atribua a média calculada a
// um ponteiro para o tipo adequado. Após isso, efetue comparações com o
// valor da média e informe se o aluno foi aprovado ou reprovado mediante
// média >=7. No final, o algoritmo deve imprimir a média alocada no ponteiro,
// usando uma variável normal.

#include <stdio.h>

void main()
{

    float a;
    float b;
    float c;
    scanf("%f", &a);
    scanf("%f", &b);
    scanf("%f", &c);
    float media = (a+b+c)/3;
    float *mediaPont = &media;

    if(*mediaPont>=7){
        printf("Aprovado\n");
    }
    else{
        printf("reprovado\n");
    }

    printf("media: %.2f", media);



}
