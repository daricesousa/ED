// 3 – Desenvolva uma função (obedecendo o escopo abaixo) que leia uma série
// de N números informados pelo usuário e retorne os valores de:
// • Media, maior numero, menor numero e variancia
// • Float* teste_parametros(int N, int *maiorNumero, int *menorNumero);

#include <stdio.h>

void teste_parametros(int N, int *maiorNumero, int *menorNumero, float *media, float *variancia)
{
    int vetor[N];
    for (int i = 0; i < N; i++)
    {
        scanf("%d", &vetor[i]);
        if (i == 0)
        {
            *maiorNumero = vetor[0];
            *menorNumero = vetor[0];
            *media = vetor[0];
        }
        else
        {
            if (vetor[i] > *maiorNumero)
            {
                *maiorNumero = vetor[i];
            }
            if (vetor[i] < *menorNumero)
            {
                *menorNumero = vetor[i];
            }
            *media += vetor[i];
        }
    }
    *media /= N;

    for (int i = 0; i < N; i++)
    {
        *variancia = (vetor[i] - *media) * (vetor[i] - *media);
    }
    *variancia/= N;
}

void main()
{
    int N;
    printf("Digite a quantidade de números que deseja informar:");
    scanf("%d", &N);
    int maiorNumero;
    int menorNumero;
    float media;
    float variancia;
    teste_parametros(N, &maiorNumero, &menorNumero, &media, &variancia);
    printf("maior valor: %d\nmenor valor: %d\nmedia: %.2f\nvariancia: %.2f\n", maiorNumero, menorNumero, media, variancia);
}
