// Protótipo da função: float harmonica(int n, float *v)
// Protótipo da função: float geometrica(int n, float *v);
// • Protótipo da função: float geometrica(int n, float *v, float *w);

#include <stdio.h>
#include <math.h>

float harmonica(int n, float *v){
    float resultado = 0;
    for(int i=0; i<n; i++){
        resultado += 1/v[i];
    }
    return 1/resultado;
}

float geometrica(int n, float *v){
    float resultado = 1;
    for(int i=0; i<n; i++){
        resultado *= v[i];
    }
    return pow(resultado, 1.0/n);
}

float ponderada(int n, float *v, float *w){
    float resultado = 0;
    float soma = 0;
    for(int i=0; i<n; i++){
        resultado += v[i]*w[i];
        soma += w[i];
    }
    return resultado/soma;
}

void main(){
    float v[5] = {2,3,5,6,9};
    float w[5] = {1,1,3,2,2};
    int n=5;

    printf("%.2f\n", harmonica(5, v));
    printf("%.2f\n", geometrica(5, v));
    printf("%.2f\n", ponderada(5, v, w));
}