#include <stdio.h>

void somaProduto(int * soma, int * produto, int a, int b, int c){
    *soma = a + b + c;
    *produto = a * b * c;
}

void main(){
    int a, b, c, soma, produto;
    scanf("%d %d %d", &a, &b, &c);
    somaProduto(&soma, &produto, a, b, c);
    printf("soma: %d\nproduto: %d\n", soma, produto);
}
