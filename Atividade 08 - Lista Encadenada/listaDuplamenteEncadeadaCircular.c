#include <stdio.h>
#include <stdlib.h>

struct no
{
    int valor;
    struct no *ant;
    struct no *prox;
};
typedef struct no No;

struct lista
{
    No *inicio;
    No *fim;
};
typedef struct lista Lista;

Lista *criarLista()
{
    return NULL;
}

Lista *adicionarElemento(Lista *lista, int elemento)
{
    No *novo;
    novo = (No *)malloc(sizeof(No));
    novo->valor = elemento;
    if (lista->inicio == NULL)
    {
        novo->ant = novo;
        novo->prox = novo;
        lista->inicio = novo;
        lista->fim = novo;
    }
    else
    {
        No *aux;
        aux = (No *)malloc(sizeof(No));
        aux = lista->inicio;
        while (aux->prox->valor < elemento && aux->prox != lista->inicio)
        {
            aux = aux->prox;
        }
        //Só há um termo na lista
        if (lista->fim == lista->inicio)
        {
            aux->prox = novo;
            aux->ant = novo;
            novo->prox = aux;
            novo->ant = aux;
            //o valor será o ultimo
            if (aux->valor < elemento)
            {
                lista->fim = novo;
            }
            //o valor será o primeiro
            else
            {
                lista->inicio = novo;
            }
        }
        else
        {
            // o valor será o último
            if (aux == lista->fim)
            {
                lista->fim = novo;
            }
            //o valor será o primeiro
            else if (aux == lista->inicio && lista->inicio->valor > elemento)
            {
                aux = aux->ant;
                lista->inicio = novo;
            }
            novo->prox = aux->prox;
            aux->prox = novo;
            novo->ant = aux;
            novo->prox->ant = novo;
        }
    }

    return lista;
}

void imprimir(Lista *lista)
{
    if (lista->inicio != NULL)
    {
        No *aux;
        aux = lista->inicio;
        do
        {
            printf("%d ", aux->valor);
            aux = aux->prox;
        } while (aux != lista->inicio);
    }
    else
    {
        printf("A lista está vazia\n");
    }
}

No *buscar(Lista *lista, int valor)
{
    No *aux;
    aux = lista->inicio;
    do
    {
        if (aux->valor == valor)
        {
            break;
        }
        aux = aux->prox;
    } while (aux != lista->inicio);
    if (aux == lista->inicio && aux->valor != valor)
    {
        return NULL;
    }
    return aux;
}

Lista *remover(Lista *lista, int valor)
{
    No *aux = buscar(lista, valor);
    if (aux == NULL)
    {
        printf("\nValor não pertence a lista\n");
        return lista;
    }
    //só há 1 item na lista
    if (lista->inicio == lista->fim)
    {
        lista->inicio = NULL;
        lista->fim = NULL;
        printf("\n%d removido\n", valor);
        free(aux);
    }
    else
    {
        if (aux == lista->fim)
        {
            lista->fim = aux->ant;
        }
        else if (aux == lista->inicio)
        {
            lista->inicio = aux->prox;
        }
        aux->ant->prox = aux->prox;
        aux->prox->ant = aux->ant;
        printf("\n%d removido\n", valor);
        free(aux);
    }
    return lista;
}

void contar(Lista *lista)
{
    int cont = 0;
    if (lista->inicio != NULL)
    {
        No *aux;
        aux = lista->inicio;
        do
        {
            cont++;
            aux = aux->prox;
        } while (aux != lista->inicio);
    }

    printf("\nA lista contém %d elementos\n", cont);
}

void main()
{
    Lista *lista = (Lista *)malloc(sizeof(Lista));
    lista->fim = NULL;
    lista->inicio = NULL;
    lista = adicionarElemento(lista, 12);
    lista = adicionarElemento(lista, 20);
    lista = adicionarElemento(lista, 15);
    lista = remover(lista, 12);
    imprimir(lista);
    contar(lista);
}
