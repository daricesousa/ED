// Discuta, passo a passo, o efeito do seguinte fragmento de código:

    //sao declarados dois ponteiros: p e q
    int *p, *q;

    // realoco o tamanho de p
    p = (int*) malloc(sizeof(int));

    // o espaço de memoria que o ponteiro p aponta recebe o valor de 123
    *p = 123;

    // realoco o tamanho de q
    q = malloc(sizeof(int));

    // o espaço de memoria que o ponteiro q aponta
    //recebe o valor do espaço de memoria que o vetor p aponta, ou seja, 123
    *q = *p;

    //o ponteiro q passa a apontar para o ponteiro p
     q = p;

    //libera o espaço de memoria do ponteiro p
    free(p);

    //tenta liberar o mesmo espaço de memoria que ja foi liberado
    free(q);

    //q recebe null
    q = NULL;
