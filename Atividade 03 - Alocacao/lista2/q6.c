// (UFOP) Construa um programa (main) que aloque em tempo de execução (dinamicamente) uma
// matriz de ordem m x n (linha por coluna), usando 2 chamadas à função malloc. Agora, aproveite
// este programa para construir uma função que recebendo os parâmetros m e n aloque uma matriz de
// ordem m x n e retorne um ponteiro para esta matriz alocada. Crie ainda uma função para liberar a
// área de memória alocada pela matriz. Finalmente, crie um novo programa(main) que teste/use as
// duas funções criadas acima.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int **criarMatriz(int m, int n)
{
    int **matriz;
    matriz = (int **)calloc(m, sizeof(int *));
    for (int i = 0; i < m; i++)
    {
        matriz[i] = (int *)calloc(n, sizeof(int));
    }
    return matriz;
}

void liberarMemoria(int **matriz, int m)
{
    for (int i = 0; i < m; i++)
    {
        free(matriz[i]);
    }
    free(matriz);
}

void preencherMatriz(int m, int n, int **matriz)
{
    srand(time(NULL));
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
            matriz[i][j] = rand() % 100;
    }
}

void imprimirMatriz(int **Matriz, int m, int n)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            printf("%d, ", Matriz[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void main()
{
    int m, n;
    scanf("%d %d", &m, &n);
    int **matriz = criarMatriz(m, n);
    preencherMatriz(m, n, matriz);
    imprimirMatriz(matriz, m, n);
    liberarMemoria(matriz, m);
}
