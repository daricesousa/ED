// Escreva uma função que receba 2 vetores inteiros, v1 e v2, e suas respectivas quantidades de
// elementos q1 e q2, que retorne um ponteiro para uma matriz, de ordem q1 x q2, alocada
// dinamicamente, que contenha a multiplicação de cada elemento de v1 pelos elementos de v2.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int *criarVetor(int n)
{
    int *pont;
    pont = (int *)calloc(n, sizeof(int));
    return pont;
}

void imprimirVetor(int *vetor, int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("%d, ", vetor[i]);
    }
    printf("\n");
}

void liberarVetor(int *vetor)
{
    free(vetor);
}

int *juntarVetores(int *v1, int *v2, int q1, int q2)
{
    int *pont = calloc(q1 * q2, sizeof(int));
    int index = 0;
    for (int i = 0; i < q1; i++)
    {
        for (int j = 0; j < q2; j++)
        {
            pont[index] = v1[i] * v2[j];
            index ++;
        }
    }
    return pont;
}

int main()
{
    int q1, q2;
    scanf("%d", &q1);
    scanf("%d", &q2);
    int *vetor1 = criarVetor(q1);
    int *vetor2 = criarVetor(q2);
    srand(time(NULL));
    for (int i = 0; i < q1; i++)
    {
        vetor1[i] = rand() % 100;
    }
    for (int i = 0; i < q2; i++)
    {
        vetor2[i] = rand() % 100;
    }
    int *pont = juntarVetores(vetor1, vetor2, q1, q2);

    imprimirVetor(pont, q1 * q2);
    liberarVetor(vetor1);
    liberarVetor(vetor2);
    liberarVetor(pont);
}