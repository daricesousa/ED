// Altere a questão anterior para que possa ajudar não o professor, mas os monitores no
// acompanhamento dos exercícios propostos. Seu programa não precisa armazenar notas, mas a
// quantidade e quais questões os alunos tiveram dificuldade num dado exercício. Associe, da maneira
// que preferir, os exercícios e questões aos alunos e escreva funções para recuperar: Quais questões
// um dado aluno teve dificuldade; E/Ou Quais alunos dada questão apresentou dificuldade;


#include <stdio.h>
#include <stdlib.h>

struct alunos
{
    char *nome;
    int quantQuestoes;
    int *questoes;
};

int menu()
{
    int op;
    printf("\n\n---------MENU----------\n");
    printf("0 - Sair\n");
    printf("1 - Cadastrar alunos\n");
    printf("2 - Cadastrar dificuldades dos alunos cadastrados\n");
    printf("3 - Mostrar alunos e suas dificuldades\n");
    scanf("%d", &op);
    return op;
}

char *alocarChar(int t)
{
    char *string = (char *)malloc(sizeof(char) * t);
    return string;
}

int *alocarQuestoes(int t)
{
    int *vetor = (int *)malloc(sizeof(int) * t);
    return vetor;
}

struct alunos *cadastrarAlunos(struct alunos *a, int *quantidade)
{
    struct alunos *alunosNovos = (struct alunos *)realloc(a, sizeof(struct alunos) * (1 + (*quantidade)));
    printf("Nome ");
    alunosNovos[*quantidade].nome = alocarChar(100);
    scanf("%s", alunosNovos[*quantidade].nome);
    (*quantidade)++;
    return alunosNovos;
}

void *cadastrarNotas(struct alunos *a, int quantidadeAlunos, int *quantidadeDificuldadesCadastradas)
{
    int i = *quantidadeDificuldadesCadastradas;
    for (i; i < quantidadeAlunos; i++)
    {
        printf("Digite a quantidade de questões que o aluno %s teve dificuldade: ", a[i]. nome);
        scanf("%d", &a[i].quantQuestoes);
        printf("Digite o número das questões que o aluno %s teve dificuldade: ", a[i]. nome);
        a[i].questoes = alocarQuestoes(a[i].quantQuestoes);
        for (int j = 0; j < a[i].quantQuestoes; j++)
        {
            scanf("%d", &a[i].questoes[j]);
        }
    }
    *quantidadeDificuldadesCadastradas = quantidadeAlunos;
}

void mostrarAlunosDificuldades(struct alunos *alunos, int quantAlunos)
{
    for (int i = 0; i < quantAlunos; i++)
    {
        printf("\n\nNome: %s\n", alunos[i].nome);
        printf("Questoes com dificuldade: ");
        for(int j=0; j<alunos[i].quantQuestoes; j++){
            printf("%d, ", alunos[i].questoes[j]);
        }
    }
}


void main()
{
    struct alunos *alunos = NULL;

    int quantAlunos = 0;
    int quantDificuldadesCadastradas = 0;
    int op = menu();

    while (1)
    {
        switch (op)
        {
        case 1:
            alunos = cadastrarAlunos(alunos, &quantAlunos);
            break;
        case 2:
            cadastrarNotas(alunos, quantAlunos, &quantDificuldadesCadastradas);
            break;
        case 3:
            mostrarAlunosDificuldades(alunos, quantAlunos);
            break;
        case 0:
            free(alunos);
            exit(0);
            break;
        default:
            printf("Valor invalido \n");
            break;
        }
        op = menu();
    };
}