
// Escreva uma função que receba um valor N e aloque dinamicamente um vetor de inteiros com N
// elementos, uma função que libere o espaço de memória utilizado, um função que imprima os
// elementos de um vetor na tela e, por fim, realize testes de todas as funções anteriores em main.

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int * criarVetor(int n){
    int * pont;
    pont = (int *) calloc(n, sizeof(int));
    return pont;
}

void imprimirVetor(int * vetor, int n){
    for(int i=0; i<n; i++){
        printf("%d, ", vetor[i]);
    }
}

void liberarVetor(int *vetor){
    free(vetor);
}

int main(){
    int n;
    scanf("%d", &n);
    int * vetor = criarVetor(n);
    srand(time(NULL));
    for(int i=0; i<n; i++){
        vetor[i] = rand() % 100;
    }
    imprimirVetor(vetor, n);
    liberarVetor(vetor);
}