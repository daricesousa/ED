// Escreva uma função que receba uma String (cadeia de caracteres) e um caractere, percorra a
// String, conte a quantidade de vezes que o caractere se repete, armazene em um vetor as posições
// que o caractere foi encontrado e retorne um ponteiro para o vetor alocado.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int *buscarCaracterer(char *string, char caracter)
{
    int *pont;
    pont = (int *)realloc(NULL, sizeof(int));
    int quant = 0;
    for (int i = 0; i < strlen(string); i++)
    {
        if (caracter == string[i])
        {
            pont[quant] = i;
            quant++;
            pont = (int *)realloc(pont, quant * sizeof(int));
        }
    }
    pont[quant] = '\n';
    return pont;
}

void main()
{
    char string[] = "parabens";
    int *vetor = buscarCaracterer(string, 'a');
    for (int i = 0; vetor[i] != '\n'; i++)
    {
        printf("%d, ", vetor[i]);
    }
}