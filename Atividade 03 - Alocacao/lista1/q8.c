// Implemente a função char *inverte(char *s) que cria dinamicamente uma nova
// string contendo a string s invertida.


#include <stdio.h>
#include <stdlib.h>

char *criarVetor(int n)
{
    char *pont;
    pont = (char *)calloc(n, sizeof(char));
    return pont;
}

void inverterVetor(char *vetor, int n)
{
    if (n % 2 == 0)
    {
        for (int i = 0; i <= n/2-1; i++)
        {
            int aux = vetor[i];
            vetor[i] = vetor[n-i-1];
            vetor[n-i-1] = aux;
        }
    }
    if (n % 2 == 1)
    {
        for (int i = 0; i <= (n-1)/2; i++)
        {
            char aux = vetor[i];
            vetor[i] = vetor[n-i-1];
            vetor[n-i-1] = aux;
        }
    }
}

void imprimirVetor(char * vetor, int n){
    for(int i=0; i<n; i++){
        printf("%c, ", vetor[i]);
    }
}

void liberarVetor(char *vetor)
{
    free(vetor);
}

int main()
{
    int n;
    scanf("%d", &n);
    char *vetor = criarVetor(n);
    scanf("%s", vetor);
    imprimirVetor(vetor, n);
    inverterVetor(vetor, n);
    printf("\n");
    imprimirVetor(vetor, n);
    liberarVetor(vetor);
}