// Escreva uma função que receba um ponteiro V com 10 posições alocadas. A função
// deve retornar o ponteiro V com o valor de cada posição multiplicado pela sua terça
// parte. Escreva uma função main para testar o código.

#include<stdio.h>
#include<stdlib.h>

float * criarVetor(int n){
    float * pont;
    pont = (float *) calloc(n, sizeof(float));
    return pont;
}

float *preencherVetor(float * vetor, int n){
    for(int i=0; i<n; i++){
       vetor[i] = (i*i)/3.0;
    }
return vetor;
}

void imprimirVetor(float * vetor, int n){
    for(int i=0; i<n; i++){
        printf("%f, ", vetor[i]);
    }
}

void liberarVetor(float *vetor){
    free(vetor);
}

int main(){
    int n=10;
    float * vetor = criarVetor(n);
    vetor = preencherVetor(vetor, n);
    imprimirVetor(vetor, n);
    liberarVetor(vetor);
}