// Faça um programa que leia um valor n e crie dinamicamente um vetor de n
// elementos e passe esse vetor para uma função que vai ler os elementos desse vetor.
// Depois, no programa principal, o vetor preenchido deve ser impresso. Além disso,
// antes de finalizar o programa, deve-se liberar a área de memória alocada.


#include<stdio.h>
#include<stdlib.h>


void preencherVetor(int * vetor, int n){
    for(int i=0; i<n; i++){
        scanf("%d", &vetor[i]);
    }
}

int main(){
    int * pont, n;

    scanf("%d", &n);
    pont = (int *) calloc(n, sizeof(int));

    preencherVetor(pont, n);

    for(int i=0; i<n; i++){
        printf("%d, ", pont[i]);
    }

    free(pont);
}