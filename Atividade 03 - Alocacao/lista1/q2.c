// Faça uma função que receba um valor n e crie dinamicamente um vetor de n
// elementos e retorne um ponteiro. Crie uma função que receba um ponteiro para um
// vetor e um valor n e imprima os n elementos desse vetor. Construa também uma
// função que receba um ponteiro para um vetor e libere esta área de memória. Ao final,
// crie uma função principal que leia um valor n e chame a função criada acima. Depois, a
// função principal deve ler os n elementos desse vetor. Então, a função principal deve
// chamar a função de impressão dos n elementos do vetor criado e, finalmente, liberar a
// memória alocada através da função criada para liberação.

#include<stdio.h>
#include<stdlib.h>


int * criarVetor(int n){
    int * pont;
    pont = (int *) calloc(n, sizeof(int));
    return pont;
}

void imprimirVetor(int * vetor, int n){
    for(int i=0; i<n; i++){
        printf("%d, ", vetor[i]);
    }
}

void liberarVetor(int *vetor){

    free(vetor);
}

int main(){
    int n;
    scanf("%d", &n);
    int * vetor = criarVetor(n);
    for(int i=0; i<n; i++){
        scanf("%d", &vetor[i]);
    }
    imprimirVetor(vetor, n);
    liberarVetor(vetor);
}