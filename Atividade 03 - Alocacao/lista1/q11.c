// Desenvolva um programa que calcule a soma de duas matrizes MxN de números
// reais (double). A implementação deste programa deve considerar as dimensões
// fornecida pelo usuário (Dica: represente a matriz através de variáveis do tipo
// double **, usando alocação dinâmica de memória).
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

double **criarMatriz(int m, int n)
{
    double **matriz;
    matriz = (double **)calloc(m, sizeof(double *));
    for (int i = 0; i < m; i++)
    {
        matriz[i] = (double *)calloc(n, sizeof(double));
    }
    return matriz;
}

void liberarMemoria(double **matriz, int m)
{
    for (int i = 0; i < m; i++)
    {
        free(matriz[i]);
    }
    free(matriz);
}

void preencherMatriz(int m, int n, double **matriz)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
            matriz[i][j] = rand() % 100;
    }
}

void preencherMatrizSoma(int m, int n, double **matriz1, double**matriz2, double **soma)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
            soma[i][j] = matriz1[i][j] + matriz2[i][j];
    }
}

void imprimirMatriz(double **Matriz, int m, int n)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            printf("%f, ", Matriz[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void main()
{
    srand(time(NULL));
    int m, n;
    scanf("%d %d", &m, &n);
    double **matriz1 = criarMatriz(m, n);
    double **matriz2 = criarMatriz(m, n);
    double **matrizSoma = criarMatriz(m, n);
    preencherMatriz(m, n, matriz1);
    preencherMatriz(m, n, matriz2);
    preencherMatrizSoma(m, n, matriz1, matriz2, matrizSoma);
    imprimirMatriz(matriz1, m, n);
    imprimirMatriz(matriz2, m, n);
    imprimirMatriz(matrizSoma, m, n);
    liberarMemoria(matrizSoma, m);
}
