// Crie uma função que aloque um vetor de inteiros dinamicamente e em seguida,
// preencha o mesmo e inverta a ordem dos seus valores.

#include <stdio.h>
#include <stdlib.h>

int *criarVetor(int n)
{
    int *pont;
    pont = (int *)calloc(n, sizeof(int));
    return pont;
}

void preencherVetor(int *vetor, int n)
{
    for (int i = 0; i < n; i++)
    {
        vetor[i] = i;
    }
}

void inverterVetor(int *vetor, int n)
{
    if (n % 2 == 0)
    {
        for (int i = 0; i <= n/2-1; i++)
        {
            int aux = vetor[i];
            vetor[i] = vetor[n-i-1];
            vetor[n-i-1] = aux;
        }
    }
    if (n % 2 == 1)
    {
        for (int i = 0; i <= (n-1)/2; i++)
        {
            int aux = vetor[i];
            vetor[i] = vetor[n-i-1];
            vetor[n-i-1] = aux;
        }
    }
}

void imprimirVetor(int * vetor, int n){
    for(int i=0; i<n; i++){
        printf("%d, ", vetor[i]);
    }
}

void liberarVetor(int *vetor)
{
    free(vetor);
}

int main()
{
    int n;
    scanf("%d", &n);
    int *vetor = criarVetor(n);
    preencherVetor(vetor, n);
    imprimirVetor(vetor, n);
    inverterVetor(vetor, n);
    printf("\n");
    imprimirVetor(vetor, n);

    
    liberarVetor(vetor);
}