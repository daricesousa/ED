// Faça um programa que aloque continuamente os valores inteiros digitados pelo
// usuário em um vetor. A alocação deve terminar quando um número negativo for
// inserido. Após, imprima na tela o vetor criado com as inserções.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int *criarVetor()
{
    int *pont;
    int num;
    int quant = 0;
    do
    {
        scanf("%d", &num);
        if (quant == 0)
        {
            quant++;
            pont = (int *)calloc(1, sizeof(int));
            pont[quant-1] = num;
        }
        else if (quant > 0)
        {
            quant++;
            pont = (int *)realloc(pont, quant * sizeof(int));
            pont[quant-1] = num;
        }

    } while (num >= 0);
    return pont;
}

void liberarVetor(int *vetor)
{
    free(vetor);
}

int main()
{

    int *vetor = criarVetor();
    for (int i = 0; vetor[i] >= 0; i++)
    {
        printf("%d, ", *(vetor + i));
    }
    liberarVetor(vetor);
}