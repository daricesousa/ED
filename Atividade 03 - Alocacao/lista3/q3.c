// Crie uma função que percorra um determinado vetor alocado dinamicamente
// sem utilizar índices, apenas com a aritmética de ponteiros.

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int * criarVetor(int n){
    int * pont;
    pont = (int *) calloc(n, sizeof(int));
    return pont;
}

void imprimirVetor(int * vetor, int n){
    for(int i=0; i<n; i++){
        printf("%d, ", *(vetor+i));
    }
}

void liberarVetor(int *vetor){
    free(vetor);
}

int main(){
    int n;
    scanf("%d", &n);
    int * vetor = criarVetor(n);
    srand(time(NULL));
    for(int i=0; i<n; i++){
        *(vetor+i) = rand() % 100;
    }
    imprimirVetor(vetor, n);
    liberarVetor(vetor);
}