// Crie um programa que, dadas 6 strings inseridas pelo usuário (mínimo 4
// caracteres) alocadas dinamicamente (podem ser alocadas em uma matriz de
// caracteres ou 6 vetores), seja capaz de exibir qual a menor e qual a maior entre
// as strings inseridas.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main()
{
    int i;
    char **palavras = (char **)calloc(6, sizeof(char *));
    char *palavraMenor, *palavraMaior;

    for (i = 0; i < 6; i++)
    {
        char palavra[100];
        scanf("%s", palavra);
        int tamanho = strlen(palavra);
        palavras[i] = (char *)calloc(tamanho, sizeof(char));
        strcpy(palavras[i], palavra);

        if (i == 0)
        {
            palavraMenor = (char *)calloc(tamanho, sizeof(char));
            palavraMaior = (char *)calloc(tamanho, sizeof(char));
            strcpy(palavraMenor, palavra);
            strcpy(palavraMaior, palavra);
        }
        else if (tamanho > strlen(palavraMaior))
        {
            palavraMaior = (char *)calloc(tamanho, sizeof(char));
            strcpy(palavraMaior, palavra);
        }
        else if (tamanho < strlen(palavraMenor))
        {
            palavraMenor = (char *)calloc(tamanho, sizeof(char));
            strcpy(palavraMenor, palavra);
        }
    }
    printf("Maior: %s\n", palavraMaior);
    printf("Menor: %s\n", palavraMenor);
}