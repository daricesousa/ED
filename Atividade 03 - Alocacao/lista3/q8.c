// Crie um algoritmo que, dados dois valores N e M inseridos pelo usuário, crie a
// matriz MxN alocada dinamicamente, preencha-a (com valores aleatórios ou
// inseridos pelo usuário) e logo após, aloque e preencha a sua matriz transposta e
// em seguida imprima ambas.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int **criarMatriz(int m, int n)
{
    int **matriz;
    matriz = (int **)calloc(m, sizeof(int *));
    for (int i = 0; i < m; i++)
    {
        matriz[i] = (int *)calloc(n, sizeof(int));
    }
    return matriz;
}

void liberarMemoria(int **matriz, int m)
{
    for (int i = 0; i < m; i++)
    {
        free(matriz[i]);
    }
    free(matriz);
}

void preencherMatriz(int m, int n, int **matriz)
{
    srand(time(NULL));
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
            matriz[i][j] = rand() % 100;
    }
}

void preencherMatrizT(int m, int n, int **A, int **T){
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            T[j][i] = A[i][j];
        }
    }
}



void imprimirMatriz(int **Matriz, int m, int n)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            printf("%d, ", Matriz[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void main()
{
    int m, n;
    scanf("%d %d", &m, &n);
    int **matriz = criarMatriz(m, n);
    preencherMatriz(m, n, matriz);
    int **matrizT = criarMatriz(n, m);
    preencherMatrizT(m, n, matriz, matrizT);
    imprimirMatriz(matriz, m, n);
    printf("\n");
    imprimirMatriz(matrizT, n, m);
    liberarMemoria(matriz, m);
}
