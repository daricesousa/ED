// Crie uma função que, dadas duas strings S1 e S2 alocadas dinamicamente, troque
// seus conteúdos e imprima na tela o resultado da troca.


#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include <string.h>

char * criarVetor(int n){
    char * pont;
    pont = (char *) calloc(n, sizeof(char));
    return pont;
}

void imprimirVetor(char * vetor, int n){
    for(int i=0; i<n; i++){
        printf("%c, ", vetor[i]);
    }
}

void liberarVetor(char *vetor){
    free(vetor);
}

int main(){
    int n = 6;
    char * string1 = criarVetor(n);
    char * string2 = criarVetor(n);
    strcpy(string1, "Onibus");
    strcpy(string2, "Parada");
    char auxiliar[n];
    strcpy(auxiliar, string1);
    strcpy(string1, string2);
    strcpy(string2, auxiliar);
    imprimirVetor(string1, n);
    printf("\n");
    imprimirVetor(string2, n);
    liberarVetor(string1);
    liberarVetor(string2);
}