// Considerando a existência de duas matrizes de números inteiros (A[5][5] e
// B[5][5]), desenvolva uma função que retorne a soma entre estas duas matrizes
// usando alocação dinâmica de memória. Após a exibição, libere a memória
// ocupada pelas três matrizes criadas.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int **criarmatriz(int m, int n)
{
    int **matriz;
    matriz = (int **)calloc(m, sizeof(int *));
    for (int i = 0; i < m; i++)
    {
        matriz[i] = (int *)calloc(n, sizeof(int));
    }
    return matriz;
}

void preencherMatriz(int m, int n, int **matriz)
{
    srand(time(NULL));
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
            matriz[i][j] = rand() % 100;
    }
}

void imprimirMatriz(int **Matriz, int m, int n)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            printf("%d, ", Matriz[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void liberarMatriz(int **matriz, int m)
{
    for (int i = 0; i < m; i++)
    {
        free(matriz[i]);
    }
    free(matriz);
}

int **juntarMatrizes(int **matrizSoma, int **matriz1, int **matriz2, int m, int n)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            matrizSoma[i][j] = matriz1[i][j] + matriz2[i][j];
        }
    }
    return matrizSoma;
}

int main()
{
    int **matriz1 = criarmatriz(5, 5);
    int **matriz2 = criarmatriz(5, 5);
    preencherMatriz(5, 5, matriz1);
    preencherMatriz(5, 5, matriz2);

    int **matrizSoma = criarmatriz(5, 5);

    matrizSoma = juntarMatrizes(matrizSoma, matriz1, matriz2, 5, 5);

    imprimirMatriz(matrizSoma, 5, 5);
    liberarMatriz(matriz1, 5);
    liberarMatriz(matriz2, 5);
    liberarMatriz(matrizSoma, 5);
}