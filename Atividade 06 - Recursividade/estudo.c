#include <stdio.h>

int fatorial(int n){
    if(n == 1 || n==0){
        return 1;
    }
    return n * fatorial(n-1);
}

int somaArray(int vet[], int tamanho){
    tamanho -= 1;
    if(tamanho == 0){
        return vet[tamanho];
    }
    return vet[tamanho]+ somaArray(vet, tamanho);
}

int fibbonacci(int n){
    if(n == 0 | n == 1)
        return n;
    return (fibbonacci(n-1) + fibbonacci(n-2));

}
 
void main(){
    printf("%d\n", fatorial(3));

    int vetor[] = {4,3,10};
    printf("%d\n", somaArray(vetor, 3));

    int quantidadeTermos = 10;
    printf("%d\n",fibbonacci(quantidadeTermos));
    for(int i=0; i<= quantidadeTermos; i++){
        printf("%d ", fibbonacci(i));
    }
}