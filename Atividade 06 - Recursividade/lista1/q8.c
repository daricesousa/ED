// Escreva uma função recursiva que calcule a soma dos dígitos de um número inteiro. Por
// exemplo, se a entrada for 123, a saída deverá ser 1+2+3 = 6.

#include <stdio.h>

int somaDigitos(int n){
    int resto = n % 10;
    n = n/10;
    if(n == 0){
        return resto;
    }
    return resto + somaDigitos(n);
}

void main(){
    printf("%d", somaDigitos(3451));
}