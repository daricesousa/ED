// Escreva uma função recursiva que receba um valor inteiro x e o retorne invertido. Exemplo: se x
// = 123, a função deve retornar 321.

#include <stdio.h>

int inverter(int n, int novoN){
    int resto = n % 10;
    n = n/10;
    novoN = novoN*10 + resto;
    if(n == 0){
        return novoN;
    }
    return inverter(n, novoN);
}

void main(){
    printf("%d", inverter(123, 0));
}