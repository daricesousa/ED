// Escreva uma função recursiva que receba dois valores inteiros (x e y) e retorne o resultado de
// x^y para o método principal.

#include <stdio.h>

int expo(int b, int p){
    if(p == 1){
        return b;
    }
    if(p == 0){
        return 1;
    }
    return b * expo(b, p-1);
}

void main(){
    printf("%d", expo(3,3));
}