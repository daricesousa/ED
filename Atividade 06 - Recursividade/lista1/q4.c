// Escreva uma função recursiva que retorne quantas vezes um número X aparece em um dado
// número natural. Por exemplo, o número 3 aparece 2 vezes no número 45303.

#include <stdio.h>

int buscar(int n, int busca){
    int resto = n % 10;
    n = n/10;
    int apareceu = 0;
    if(resto == busca){
        apareceu = 1;
    }
    if(n == 0){
        return apareceu;
    }
    return apareceu + buscar(n, busca);
}

void main(){
    printf("%d", buscar(3450,3));
}