// Crie um programa em C, que contenha uma função recursiva para encontrar o menor elemento
// em um vetor. A leitura dos elementos do vetor e impressão do menor elemento devem ser feitas no
// programa principal.
#include <stdio.h>

int menorNumero(int vet[], int tamanho){
    tamanho -= 1;
    if(tamanho == 0){
        return vet[tamanho];
    }
    if(vet[tamanho]< vet[tamanho-1]){
        vet[tamanho-1]=vet[tamanho];
    }
    return menorNumero(vet, tamanho);
}

void main(){
    int vetor[] = {4,3, 7, 2,10};
    printf("%d\n", menorNumero(vetor, 5));
}