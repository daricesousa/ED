1 - Implemente uma função recursiva que, dados dois números inteiros x e n, calcula o
valor de x^n .
2 – Implemente uma função recursiva soma (n) que calcula o somatório dos n primeiros
números inteiros. Escreva e resolva a equação de recorrência dessa função.
3 – Escreva uma função recursiva maxmin que calcule o valor de um elemento máximo
e o valor de um elemento mínimo de um vetor.
4 – Qual o resultado da execução do seguinte programa?
int ff (int n) {
if (n == 1) return 1;
if (n % 2 == 0) return ff (n/2);
return ff ((n-1)/2) + ff ((n+1)/2);
}
int main (void) {
printf ("%d", ff(7));
return 0;
}
5 – Escreva uma função recursiva que calcule o produto de todos os números que estão
presentes em uma posição par de um vetor de inteiros.
6 – Escreva uma função recursiva que calcule a média dos valores de um vetor de
inteiros.
7 – Escreva um algoritmo recursivo para avaliar a * b usando a adição, onde a e b são
inteiros não-negativos.
8 – Escreva uma função que deve ordenar um vetor de inteiros de maneira recursiva.
9 – Escreva uma função que faça a procura sequencial de um valor passado por
parâmetro num vetor também passado por parâmetro.
10 – Defina o conceito de recursividade em detalhes.
