// Escreva uma função que deve ordenar um vetor de inteiros de maneira recursiva.

#include <stdio.h>

void ordenacao(int vet[], int tamanho)
{
    if (tamanho == 0)
    {
        return;
    }
    int maior = 0;
    int posicaoMaior;
    for (int i = 0; i < tamanho; i++)
    {
        if (vet[i] > maior)
        {
            maior = vet[i];
            posicaoMaior = i;
        }
    }
    vet[posicaoMaior] = vet[tamanho - 1];
    vet[tamanho - 1] = maior;
    return ordenacao(vet, tamanho - 1);
}

void main(){
    int tamanho = 6;
    int vetor[] = {33,7,17,3,20,1};
    ordenacao(vetor, tamanho);
    for(int i=0; i<tamanho; i++){
        printf("%d ", vetor[i]);
    }
}