// Implemente uma função recursiva soma (n) que calcula o somatório dos n primeiros
// números inteiros. Escreva e resolva a equação de recorrência dessa função.

#include <stdio.h>

int soma(int k){
    if(k == 0){
        return 0;
    }
    return k + soma(k-1);

}

void main(){
    printf("%d", soma(3));
}