// Escreva um algoritmo recursivo que escreva na tela a soma
// de todos os números inteiros positivos de K até 0.


#include <stdio.h>

int soma(int k){
    if(k == 0){
        return 0;
    }
    return k + soma(k-1);

}

void main(){
    printf("%d", soma(5));
}