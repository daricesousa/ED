// Escreva uma função recursiva que calcule a soma de todos
// os números compreendidos entre os valores A e B passados
// por parâmetro.

#include <stdio.h>

int soma(int a, int b){
    if(a == b){
        return 0;
    }
    return a + soma(a + 1, b);

}

void main(){
    printf("%d", soma(1,5));
}