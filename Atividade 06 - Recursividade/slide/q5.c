// Escreva uma função recursiva que calcule os juros
// compostos de um valor. Para isso o programa deverá ler um
// valor inicial, o número de meses e a taxa de juros ao mês, e
// passar estes valores à função como parâmetros.

#include <stdio.h>

float jurosCompostos(float capital, int tempo, float taxa){
    if(tempo == 1){
        return capital;
    }
    return (1+taxa) * jurosCompostos(capital, tempo-1, taxa);
}

void main(){
    float capital = 5000;
    int tempo = 6;
    float taxa = 0.01;

    printf("%f", (jurosCompostos(capital,tempo,taxa)-capital));
}
