// Crie uma função recursiva que retorne a soma dos elementos de um vetor
// de inteiros.

#include <stdio.h>

int somaArray(int vet[], int tamanho){
    tamanho -= 1;
    if(tamanho == 0){
        return vet[tamanho];
    }
    return vet[tamanho]+ somaArray(vet, tamanho);
}

void main(){
    int vetor[] = {4,3,10};
    printf("%d\n", somaArray(vetor, 3));
}