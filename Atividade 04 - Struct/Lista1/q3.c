// Faça um programa em C que leia um conjunto de livros, onde cada um dos
// livros possui as seguintes informações: Título, Autor, Editora, ano de edição e
// números de exemplares. O usuário poderá inserir quantos livros desejar
// desde que não ultrapasse o limite máximo do conjunto. Faça uma opção onde
// o usuário digite a Editora e o programa mostre todos os livros cadastrados
// daquela editora e repita o mesmo para Autor

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct Livro
{
    char titulo[20];
    char autor[20];
    char editora[20];
    int anoPublicacao;
    int numeroExemplares;
};
typedef struct Livro Livro;

Livro *buscarAutor(Livro *livros, int n, int *quantResultadosPont)
{
    Livro *resultado;
    *quantResultadosPont = 0;
    char buscarAutor[20];
    printf("Autor: ");
    scanf("%s", buscarAutor);
    for (int i = 0; i < n; i++)
    {
        if (!strcmp(buscarAutor, livros[i].autor))
        {
            if (*quantResultadosPont == 0)
            {
                resultado = (Livro *)calloc(1, sizeof(Livro));
                *quantResultadosPont = 1;
                resultado[0] = livros[i];
            }
            else
            {
                (*quantResultadosPont)++;
                resultado = (Livro *)realloc(resultado, sizeof(Livro) * (*quantResultadosPont));
                resultado[*quantResultadosPont - 1] = livros[i];
            }
        }
    }
    return resultado;
}

Livro *buscarEditora(Livro *livros, int n, int *quantResultadosPont)
{
    Livro *resultado;
    *quantResultadosPont = 0;
    char buscarEditora[20];
    printf("Editora: ");
    scanf("%s", buscarEditora);
    for (int i = 0; i < n; i++)
    {
        if (!strcmp(buscarEditora, livros[i].editora))
        {
            if (*quantResultadosPont == 0)
            {
                resultado = (Livro *)calloc(1, sizeof(Livro));
                *quantResultadosPont = 1;
                resultado[0] = livros[i];
            }
            else
            {
                (*quantResultadosPont)++;
                resultado = (Livro *)realloc(resultado, sizeof(Livro) * (*quantResultadosPont));
                resultado[*quantResultadosPont - 1] = livros[i];
            }
        }
    }
    return resultado;
}

void main()
{
    int n = 3;
    Livro *livros = (Livro *)calloc(n, sizeof(Livro));

    printf("Cadastre os livros\n");
    for (int i = 0; i < n; i++)
    {
        printf("\nLivro %d\n", i + 1);
        printf("Titulo: ");
        scanf("%s", livros[i].titulo);

        printf("Autor: ");
        scanf("%s", livros[i].autor);

        printf("Editora: ");
        scanf("%s", livros[i].editora);

        printf("Ano de edicao: ");
        scanf("%d", &livros[i].anoPublicacao);

        printf("Numero de exemplares: ");
        scanf("%d", &livros[i].numeroExemplares);
    }

    while (1)
    {
        int op, quantResultados;
        Livro *resultado;
        printf("1 - Pesquisar por editora\n");
        printf("2 - Pesquisar por autor\n");
        printf("Opcao: ");
        scanf("%d", &op);
        if(op !=1 && op!=2)
        {
            printf("Opção invalida\n");
            break;
        }
        if (op == 1)
        {
            resultado = buscarEditora(livros, n, &quantResultados);
            printf("A busca retornou %d resultados\n", quantResultados);
        }
        if (op == 2)
        {
            resultado = buscarAutor(livros, n, &quantResultados);
            printf("A busca retornou %d resultados\n", quantResultados);
        }
        for (int i = 0; i < quantResultados; i++)
        {
            printf("\nTitulo: %s\n", resultado[i].titulo);
            printf("Autor: %s\n", resultado[i].autor);
            printf("Editora: %s\n", resultado[i].editora);
            printf("Ano de edicao: %d\n", resultado[i].anoPublicacao);
            printf("Numero de copias: %d\n", resultado[i].numeroExemplares);
            printf("------------\n");
        }
    }
}