// Faça um programa em C que leia os dados de cinco pessoas, 
// como o nome, a idade e o sexo e ao final imprima a média das 
// idades e pessoas têm idade maior que a média.

#include <stdio.h>

struct Pessoa{
    char nome[100];
    int idade;
    char sexo[10];
};

typedef struct Pessoa Pessoa;

void main(){
    Pessoa pessoa[3];
    float media = 0;
    int quantMaiorMedia =0;

  for(int i = 0; i < 3; i++){
    printf("Nome: ");
    scanf("%s", pessoa[i].nome);
    
    printf("Idade: ");
    scanf("%d", &pessoa[i].idade);
    
    printf("Sexo: ");
    scanf("%s",pessoa[i].sexo);
    
    printf("\n");
    media += pessoa[i].idade;
  }
  media = media / 3;
 
  for(int i = 0; i < 3; i++){
    if (pessoa[i].idade > media)
      quantMaiorMedia += 1;
  }
  printf("Media: %.2f", media);
  printf("\nIdades maiores que a media: %d", quantMaiorMedia);

}

