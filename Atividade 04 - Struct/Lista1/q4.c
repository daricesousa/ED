// Faça um programa em C que leia um conjunto de DVDs, onde cada um dos
// DVDs possui as seguintes informações: Código, Título, Diretor, ano. Faça
// uma opção onde permita ao usuário ver todos os DVDs de um determinado
// ano, outra que informe quais DVDs possuem título que iniciam com vogal

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

struct Dvd{
  int codigo;
  char titulo[100];
  char diretor[10];
  int ano;

};

typedef struct Dvd Dvd;

void mostrarVogal(Dvd *dvds, int size){
  for (int i = 0; i < size; i++){
      char letraInicial=  tolower(dvds[i].titulo[0]);
    if(letraInicial == 'a' || letraInicial == 'e' || letraInicial == 'i' || letraInicial == 'o' || letraInicial == 'u'){
      printf("\nTitulo: %s\n", dvds[i].titulo);
      printf("Diretor: %s\n", dvds[i].diretor);
      printf("Codigo: %d\n", dvds[i].codigo);
      printf("Ano: %d\n", dvds[i].ano);
    }
  }
}

void pesquisarPorAno(Dvd *dvds, int size){
  int ano;
  printf("Digite o ano que deseja buscar: ");
  scanf("%d",&ano);
  for (int i = 0; i < size; i++){
    if(dvds[i].ano == ano){
      printf("\nTitulo: %s\n", dvds[i].titulo);
      printf("Diretor: %s\n", dvds[i].diretor);
      printf("Codigo: %d\n", dvds[i].codigo);
      printf("Ano: %d\n", dvds[i].ano);
    }
  }
}

void main(){
  Dvd *dvds;
  int quantDvds;
  printf("Quantidade de DVDs: ");
  scanf("%d", &quantDvds);

  dvds  = (Dvd *) calloc(quantDvds, sizeof(Dvd));
  for (int i = 0; i < quantDvds; i++){
    printf("\nTitulo: ");
    setbuf(stdin, NULL);
    scanf("%[^\n]s", dvds[i].titulo);
    setbuf(stdin, NULL);

    printf("Diretor: ");
    scanf("%s", dvds[i].diretor);

    printf("Codigo: ");
    scanf("%d", &dvds[i].codigo);

    printf("Ano: ");
    scanf("%d", &dvds[i].ano);
  }

  while (1){
    int option;
    printf("\n1 - Pesquisar por ano\n");
    printf("2 - Mostrar títulos que iniciam por vogal\n");
    scanf("%d", &option);
    if(option == 1)
      pesquisarPorAno(dvds, quantDvds);
    else if (option == 2)
      mostrarVogal(dvds, quantDvds);
    else
      printf("opção invalida");
  }
}