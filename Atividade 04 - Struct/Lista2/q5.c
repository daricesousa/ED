// Vamos supor que um número real seja representado por uma estrutura em C, como
// esta: struct realtype {
// int left;
// int right;
// };
// onde left e right representam os dígitos posicionados à esquerda e à direita do ponto
// decimal, respectivamente. Se left for um inteiro negativo, o número real representado
// será negativo.
// a. Escreva uma rotina para inserir um número real e criar uma estrutura
// representando esse número.
// b. Escreva uma função que aceite essa estrutura e retorne o número real
// representado por ela.
// c. Escreva rotinas add, subtract e multiply que aceitem duas dessas estruturas e
// retorne o valor dos cálculos, que seja a soma, a diferença e o produto,
// respectivamente, dos dois registros de entrada.

#include <stdio.h>

struct realtype
{
    int left;
    int right;
};
typedef struct realtype realtype;

int contaDigitos(int n)
{
    int digitos = 0;
    if (n == 0)
    {
        digitos = 1;
    }
    while (n != 0)
    {
        digitos++;
        n /= 10;
    }
    return digitos;
}

void arrumarCasasDecimais(realtype f1, realtype f2)
{
    int decimalF1 = contaDigitos(f1.right);
    int decimalF2 = contaDigitos(f2.right);
    if (decimalF1 > decimalF2)
    {
        int diferenca = decimalF1 - decimalF2;
        f2.right *= 10 * diferenca;
    }
    if (decimalF2 > decimalF1)
    {
        int diferenca = decimalF2 - decimalF1;
        f1.right *= 10 * diferenca;
    }
}

float transformarEmFloat(realtype f)
{
    float n = f.left;
    n += f.right / 100.0;
    return n;
}

realtype transformarEmStruct(float n)
{
    realtype f;
    f.left = (int)n;
    f.right = (n - f.left) * 100;

    return f;
}

realtype subtracaoFunc(realtype f1, realtype f2, int negativo)
{
    realtype f;
    if (f1.left > f2.left || (f1.right == f2.right && f1.left >= f2.left))
    {
        f.left = f1.left - f2.left;
        {

            if (f1.right - f2.right < 0)
            {
                f1.right += 100;
                f.left--;
            }
            f.right = f1.right - f2.right;
        }
        if(negativo == 1){
            f.left *= -1;
        }
        return f;
    }
    else
    {
        subtracaoFunc(f2, f1, 1);
    }
}

realtype somaFunc(realtype f1, realtype f2)
{
    realtype f;
    f.left = f1.left + f2.left;
    f.right = f1.right + f2.right;
    if (f.right < 0)
    {
        f.left--;
        f.right += 10;
    }
    return f;
}

realtype multiplicacaoFunc(realtype f1, realtype f2)
{
    return transformarEmStruct(transformarEmFloat(f1) * transformarEmFloat(f2));
}

void main()
{
    realtype f[2];
    float n;

    printf("Digite um número no formato da estrutura:\n");
    printf("left: ");
    scanf("%d", &f[0].left);
    printf("right: ");
    scanf("%d", &f[0].right);
    printf("\n");

    printf("O número digitado é: %f\n", transformarEmFloat(f[0]));

    printf("Digite um número float: ");
    scanf("%f", &n);
    f[1] = transformarEmStruct(n);
    printf("O número digitado é: left:%d right:%d\n", f[1].left, f[1].right);
    arrumarCasasDecimais(f[0], f[1]);
    realtype soma = somaFunc(f[0], f[1]);
    realtype subtracao = subtracaoFunc(f[0], f[1], 0);
    realtype multiplicacao = multiplicacaoFunc(f[0], f[1]);
    printf("A soma dos dois números é: left:%d right:%d\n", soma.left, soma.right);
    printf("A subtracao dos dois números é: left:%d right:%d\n", subtracao.left, subtracao.right);
    printf("A multiplicacao dos dois números é: left:%d right:%d\n", multiplicacao.left, multiplicacao.right);
}