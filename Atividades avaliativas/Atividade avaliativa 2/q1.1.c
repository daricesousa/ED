#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define tam 5

typedef struct ponto ponto;
typedef struct grupo grupo;
struct ponto
{
    int x, y, valor;
};
struct grupo
{
    ponto semente;
    ponto elementos[tam * tam];
    int rotulo; //valor que identifica os pontos pertencentes ao grupo
    int qtdelementos;
};

void main()
{
    int mat[tam][tam];
    grupo grupo[tam * tam];
    int l, count = 0, elementos[tam];

    srand(time(NULL));
    for (int i = 0; i < tam; i++)
    {
        for (int j = 0; j < tam; j++)
        {
            mat[i][j] = rand() % 10;
            printf("%d ", mat[i][j]);
        }
        printf("\n");
    }
    for (int i = 0; i < 5; i++)
    {
        do
        {
            printf("Informe o valor da semente  (X,Y):");
            scanf("%d %d", &grupo[i].semente.x, &grupo[i].semente.y);
            if (grupo[i].semente.x > tam || grupo[i].semente.y > tam)
            {
                printf("Posição invalida");
            }
        } while (grupo[i].semente.x > tam || grupo[i].semente.y > tam);
        grupo[i].semente.valor = mat[grupo[i].semente.x][grupo[i].semente.y];
        printf("Valor semente: %d\n", grupo[i].semente.valor);
    }
    printf("valor de L:");
    scanf("%d", &l);

    int sub = 0;
    for (int i = 0; i < tam; i++)
    {
        for (int j = 0; j < tam; j++)
        {
            sub = grupo[i].semente.valor - mat[i][j];
            if (sub <= l && sub >= 0)
            {
                grupo[i].elementos[j].valor = mat[i][j];
                printf("mat[%d][%d] = %d\n", i, j, grupo[i].elementos[j].valor);
            }
        }
    }
}