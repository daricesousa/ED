#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

typedef struct ponto ponto;
typedef struct Grupo Grupo;
struct ponto
{
    int x, y, valor;
};
struct Grupo
{
    ponto semente;
    ponto *elementos;
    int rotulo; //valor que identifica os pontos pertencentes ao grupo
    int qtdelementos;
};

int calcDistancia(int x1, int y1, int x2, int y2)
{
    return (int)sqrt(pow(x2 - x1, 2.0) + pow(y2 - y1, 2.0));
}

void main()
{
    int tam;
    printf("Digite o tamanho da matriz:");
    scanf("%d", &tam);
    int **mat;
    mat = (int **)calloc(tam, sizeof(int *));

    for (int j = 0; j < tam; j++)
    {
        mat[j] = (int *)calloc(tam, sizeof(int));
    }

    Grupo *grupo;
    int l;

    srand(time(NULL));
    for (int i = 0; i < tam; i++)
    {
        for (int j = 0; j < tam; j++)
        {
            mat[i][j] = rand() % 256;
        }
    }

    int contGrupos = 0;
    int contTotal = 0;

    int ok = 0;
    do
    {
        int cont = 0;

        int sementeX;
        int sementeY;
        printf("valor de L:");
        scanf("%d", &l);
        do
        {
            printf("Informe o valor da semente  (X,Y):");
            scanf("%d", &sementeX);
            scanf("%d", &sementeY);
            ok = 0;
            if (sementeX >= 0 && sementeX < tam && sementeY >= 0 && sementeY < tam)
            {
                if (mat[sementeX][sementeY] < 256)
                {
                    ok = 1;
                    break;
                }
            }
            if (ok == 0)
            {
                printf("Valor inválido\n");
            }
        } while (!ok);

        if (contGrupos == 0)
        {
            grupo = (Grupo *)calloc(1, sizeof(Grupo));
            contGrupos = 1;
        }
        else
        {
            contGrupos++;
            grupo = (Grupo *)realloc(grupo, sizeof(Grupo) * contGrupos);
        }

        grupo[contGrupos - 1].rotulo = 255 + contGrupos;
        grupo[contGrupos - 1].elementos = calloc(1, sizeof(Grupo));
        grupo[contGrupos - 1].elementos[0].x = sementeX;
        grupo[contGrupos - 1].elementos[0].y = sementeY;
        grupo[contGrupos - 1].elementos[0].valor = mat[sementeX][sementeY];
        mat[sementeX][sementeY] = grupo[contGrupos - 1].rotulo;
        cont = 1;

        grupo[contGrupos - 1].semente.x = sementeX;
        grupo[contGrupos - 1].semente.y = sementeY;
        grupo[contGrupos - 1].semente.valor = mat[sementeX][sementeY];

        for (int i = 0; i < tam; i++)
        {
            for (int j = 0; j < tam; j++)
            {
                if (mat[i][j] < 256)
                {

                    if (calcDistancia(sementeX, sementeY, i, j) <= l)
                    {
                        cont++;
                        grupo[contGrupos - 1].elementos = realloc(grupo[contGrupos - 1].elementos, sizeof(Grupo) * cont);
                        grupo[contGrupos - 1].elementos[cont - 1].x = i;
                        grupo[contGrupos - 1].elementos[cont - 1].y = j;
                        grupo[contGrupos - 1].elementos[cont - 1].valor = mat[i][j];
                        mat[i][j] = grupo[contGrupos - 1].rotulo;
                    }
                }
            }
        }
        grupo[contGrupos - 1].qtdelementos = cont;
        contTotal += cont;

    } while (contTotal < tam * tam);

    for (int i = 0; i < contGrupos; i++)
    {
        printf("\nSemente: (%d,%d)\n", grupo[i].semente.x, grupo[i].semente.y);
        printf("Rotulo: %d\n", grupo[i].rotulo);
        for (int j = 0; j < grupo[i].qtdelementos; j++)
        {
            printf("(%d,%d) = %d\n", grupo[i].elementos[j].x, grupo[i].elementos[j].y, grupo[i].elementos[j].valor);
        }
    }
    printf("\n");
    for (int i = 0; i < tam; i++)
    {
        for (int j = 0; j < tam; j++)
        {
            printf("%d ", mat[i][j]);
        }
        printf("\n");
    }
}
