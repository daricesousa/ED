// Desenvolva um algoritmo que calcula a multiplicação de uma matriz por um vetor.
// Considere uma matriz quadrada. Use alocação dinâmica para o vetor e para a matriz. Ao
// final, liberar a memória.

#include <stdio.h>
#include <stdlib.h>



void main()
{
    int **matriz;
    int *vetor;
    int **matrizProduto;
    int n = 5;

    matriz = (int **)calloc(n, sizeof(int *));
    matrizProduto = (int **)calloc(n, sizeof(int *));
    vetor = (int *)calloc(n, sizeof(int));

    for (int i = 0; i < n; i++)
    {
        matriz[i] = (int *)calloc(n, sizeof(int));
        matrizProduto[i] = (int *)calloc(n, sizeof(int));
        vetor[i] = i+1;
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            matriz[i][j] = i+j+1;
        }
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            matrizProduto[i][j] = matriz[i][j] * vetor[j];
        }
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            printf("%d ", matrizProduto[i][j]);
        }
        printf("\n");
    }


    for(int i=0;i<n;i++){
        free(matrizProduto[i]);
        free(matriz[i]);
    }
    free(matriz);
    free(matrizProduto);
    free(vetor);
}