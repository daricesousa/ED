// Desenvolver um algoritmo que aloque dinamicamente uma matriz. O usuário deve
// informar o número de linhas da matriz. O número de colunas será 5x o número de linhas.
// Ao final, deve-se gerar nesse retângulo o número máximo de círculos inscritos.
// Lembrando que os círculos devem ter o mesmo raio. Ao final, liberar a memória. 

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int **alocarMatriz(int linha, int coluna)
{
    int **mat;
    int i;
    mat = (int **)calloc(linha, sizeof(int *));
    for (i = 0; i < linha; i++)
    {
        mat[i] = (int *)calloc(coluna, sizeof(int));
    }
    return mat;
}

void printMatriz(int linha, int coluna, int **mat)
{
    int i, j;
    for (i = 0; i < linha; i++)
    {
        for (j = 0; j < coluna; j++)
        {
            printf("%d ", mat[i][j]);
        }
        printf("\n");
    }
}

float calcDistancia(int x1, int x2, int y1, int y2)
{
    return (float)sqrt(pow(x2 - x1, 2.0) + pow(y2 - y1, 2.0));
}

int **calcCirculo(int linha, int coluna, int centroLinha, int centroColuna, int **mat, int raio, int n)
{
    float distancia;
    int i, j;

    for (i = 0; i < linha; i++)
    {
        for (j = 0; j < coluna; j++)
        {
            distancia = calcDistancia(i, centroLinha, j, centroColuna);
            if (distancia <= raio)
            {
                mat[i][j] = n;
            }
        }
    }
    return mat;
}

int main()
{
    int **mat;
    int linha;
    printf("Digite a quantidade de linhas da matriz: ");
    scanf("%d", &linha);
    int raio = linha / 2;
    int coluna = linha * 5;
    mat = alocarMatriz(linha, coluna);
    mat = calcCirculo(linha, coluna, raio, raio, mat, raio, 1);
    mat = calcCirculo(linha, coluna, raio, raio*3, mat, raio, 2);
    mat = calcCirculo(linha, coluna, raio, raio*5, mat, raio, 3);
    mat = calcCirculo(linha, coluna, raio, raio*7, mat, raio, 4);
    mat = calcCirculo(linha, coluna, raio, raio*9, mat, raio, 5);
    printMatriz(linha, coluna, mat);
    return 0;

    for (int i = 0; i < coluna; i++)
    {
        free(mat[i]);
    }
    free(mat);
}