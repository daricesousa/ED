#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int **alocarMatriz(int linha, int coluna)
{
    int **mat;
    int i;
    mat = (int **)calloc(linha, sizeof(int *));
    for (i = 0; i < linha; i++)
    {
        mat[i] = (int *)calloc(coluna, sizeof(int));
    }
    return mat;
}

void printMatriz(int linha, int coluna, int **mat)
{
    int i, j;
    for (i = 0; i < linha; i++)
    {
        for (j = 0; j < coluna; j++)
        {
            printf("%d", mat[i][j]);
        }
        printf("\n");
    }
}

float calcDistancia(int x1, int x2, int y1, int y2)
{
    return (float)sqrt(pow(x2 - x1, 2.0) + pow(y2 - y1, 2.0));
}

int **calcCirculo(int tamanho, int centro, int **mat, int raio, int n)
{
    float distancia;
    int i, j;

    for (i = 0; i < tamanho; i++)
    {
        for (j = 0; j < tamanho; j++)
        {
            distancia = calcDistancia(i, centro, j, centro);
            if (distancia <= raio)
            {
                mat[i][j] = n;
            }
        }
    }
    return mat;
}

int main()
{
    int **mat;
    int raio;
    int tamanho = 51;
    raio = tamanho / 2;
    mat = alocarMatriz(tamanho, tamanho);
    mat = calcCirculo(tamanho, raio, mat, raio, 1);
    mat = calcCirculo(tamanho, raio, mat, raio / 2, 2);
    printMatriz(tamanho, tamanho, mat);
    return 0;

    for (int i = 0; i < tamanho; i++)
    {
        free(mat[i]);
    }
    free(mat);
}