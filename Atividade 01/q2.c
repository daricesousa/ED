// Desenvolva uma solução que receba uma string qualquer, verifique nessa string se existe caracteres
// repetidos e mostre quais são

#include <stdio.h>

int main(){
    char string[100];
    char caracteresRepetidos[50];

    scanf("%s", string);
    for(int i=0; string[i]!= '\0'; i++){
        for(int j=i+1; string[j]!= '\0'; j++){
                if(string[i]==string[j]){
                    int jaTem = 0;
                    int k=0;
                    while(caracteresRepetidos[k]!= '\0'){
                        if(string[i] == caracteresRepetidos[k]){
                           jaTem = 1;
                        }
                        k++;
                    }
                        if(jaTem == 0){
                            caracteresRepetidos[k] = string[i];
                        }
                }
        }
    }
    for(int i=0; caracteresRepetidos[i]!= '\0'; i++){
        printf("%c",caracteresRepetidos[i]);
    }
}