// Dada uma matriz de M x N com valores randômicos preenchidos aleatoriamente no intervalo de 1 a
// 500. Após isso, o algoritmo deverá solicitar ao usuário um valor (atentar-se para obrigar o usuário a
// informar um valor dentro do intervalo). Após o usuário informar o valor, calcular as seguintes métricas:
// a. Média de todos os números maiores que o valor informado
// b. Média de todos os números menores que o valor informado
// c. Desvio padrão dos números maiores que o valor informado

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main()
{
    srand(time(NULL));
    int matriz[50][10];
    float maiores = 0;
    int qMaiores = 0;
    float menores = 0;
    int qMenores = 0;
    float somatorio = 0;
    int n;
    int total = 0;

    for (int i = 0; i < 50; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            matriz[i][j] = rand() % 500 + 1;
        }
    }
    do
    {
        printf("Digite um valor entre 1 e 500\n");
        scanf("%d", &n);
    } while (n<1 | n> 500);
    for (int i = 0; i < 50; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            if (matriz[i][j] > n)
            {
                maiores += matriz[i][j];
                qMaiores++;
            }
            if (matriz[i][j] < n)
            {
                menores += matriz[i][j];
                qMenores++;
            }
        }
    }
    float mediaMaiores = maiores / qMaiores;

    for (int i = 0; i < 50; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            if (matriz[i][j] > n)
            {
                somatorio += pow(matriz[i][j] - mediaMaiores, 2);
            }
        }
    }
    somatorio /= qMaiores;
    sqrt(somatorio);

    printf("a: %f\n", maiores/qMaiores);
    printf("b: %f\n", menores/qMenores);
    printf("c: %f\n", somatorio);
}