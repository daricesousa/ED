// Crie uma solução que, recebe do usuário 2 valores inteiros, estes dois valores devem ser alocados em
// duas variáveis comuns. Sua solução deverá efetuar a troca de valores entre essas duas variáveis sem
// utilizar uma terceira variável

#include <stdio.h>

int main(){
    int a, b;
    scanf("%d", &a);
    scanf("%d", &b);
    a = a+b;
    b = a-b;
    a = a-b;
    printf("%d %d\n", a, b);
}